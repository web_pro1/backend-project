import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  [x: string]: any;
  constructor(
    @InjectRepository(User) private usersRespository: Repository<User>,
  ) {}

  create(createUserDto: CreateUserDto) {
    return this.usersRespository.save(createUserDto);
  }

  findAll() {
    return this.usersRespository.find();
  }

  findOne(id: number) {
    return this.usersRespository.findOne({ where: { id } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersRespository.findOneBy({ id });
    if (!user) {
      throw new NotFoundException();
    }

    const updatedUser = { ...user, ...updateUserDto };
    return this.usersRespository.save(updatedUser);
  }

  async remove(id: number) {
    const user = await this.usersRespository.findOneBy({ id });
    if (!user) {
      throw new NotFoundException();
    }

    return this.usersRespository.softRemove(user);
  }
}
